Rails.application.routes.draw do
    # The priority is based upon order of creation: first created -> highest priority.
    # See how all your routes lay out with "rake routes".

    # You can have the root of your site routed with "root"
    root 'posts#list_post'

    # Example of regular route:
    #   get 'products/:id' => 'catalog#view'

    get 'post/all' => 'posts#list_post'
    get 'post/ajax_list_post' => 'posts#ajax_list_post'
    get 'post/detail/:id' => 'posts#get_detail_post'
    get 'post/category/:id' => 'posts#list_post_by_category'
    get 'post/tag/:id' => 'posts#list_post_by_tag'

    get 'comment/list_cmt' => "comments#get_list_comments"
    post 'comment/report' => "comments#report_comment"
    post 'comment/new' => 'comments#add_new'

    post 'subscriber/new' => 'subscribers#new'



    ################## REST API #################
    post 'api/admins/login' => 'admins#auth'

    post 'api/postapis/listpost' => 'postapis#get_list_post'
    post 'api/postapis/create' => 'postapis#create_post'
    post 'api/postapis/delete' => 'postapis#delete_post'
    post 'api/postapis/suggest' => 'postapis#suggest_post'

    get 'api/tagsapis/listtags' => 'tagsapis#get_list_tags'

    post 'api/cmtsapis/list' => 'commentsapis#get_list_comments'
    post 'api/cmtsapis/detail' => 'commentsapis#get_detail_cmt'
    post 'api/cmtsapis/set_status' => 'commentsapis#set_status_cmt'
    post 'api/cmtsapis/delete' => 'commentsapis#delete_cmt'
    post 'api/cmtsapis/author' => 'commentsapis#get_list_author'

    get 'api/tag/test' => 'tagsapis#test'


    post 'api/resources/images' => 'resources#get_list_images'
    post 'api/resources/images' => 'resources#get_list_images'
    post 'api/upload/images' => 'resources#upload_file'



    # Example of named route that can be invoked with purchase_url(id: product.id)
    #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

    # Example resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Example resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Example resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Example resource route with more complex sub-resources:
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', on: :collection
    #     end
    #   end

    # Example resource route with concerns:
    #   concern :toggleable do
    #     post 'toggle'
    #   end
    #   resources :posts, concerns: :toggleable
    #   resources :photos, concerns: :toggleable

    # Example resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end
end
