require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Blog
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    
    config.assets.paths << Rails.root.join("vendor", "assets", "bower_components")
    config.assets.paths << Rails.root.join("vendor", "assets", "others")
    
    config.autoload_paths << Rails.root.join('lib')

    config.autoload_paths += %W(#{config.root}/app/controllers/concerns/commons)

    config.action_view.sanitized_allowed_tags = ['strong', 'em', 'a', 'b', 'h1', 'h2', 'h3', 'h4', 'h5', 'br', 'div', 'span', 'i', 'u', 'ul', 'ol', 'li', 'script']
    config.action_view.sanitized_allowed_attributes = ['href', 'title', 'id', 'class']

    # config.force_ssl = true


    # Disqus::defaults[:account] = 'quynguyenvu'
    # Disqus::defaults[:developer] = true # if you want to test it on the localhost

  end
end
