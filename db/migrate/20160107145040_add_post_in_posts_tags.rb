class AddPostInPostsTags < ActiveRecord::Migration
    def change
        add_foreign_key :posts_tags, :tags
    end
end
