class ChangeStatusInPost < ActiveRecord::Migration
    def change
        change_column :posts, :status, :boolean, :default => 1
    end
end
