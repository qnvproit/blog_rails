class CreateComments < ActiveRecord::Migration
    def change
        create_table :comments do |t|
            t.text :content
            t.integer :post_id
            t.integer :parent_comment_id
            t.string :author
            t.boolean :status

            t.timestamps null: false
        end
    end
end
