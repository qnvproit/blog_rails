class ChangeNumberViewsInPost < ActiveRecord::Migration
    def change
        change_column :posts, :number_views, :integer, :default => 0
    end
end
