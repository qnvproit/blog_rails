class CreatePosts < ActiveRecord::Migration
    def change
        create_table :posts do |t|
            t.string :title
            t.text :content
            t.boolean :status
            t.integer :category
            t.integer :number_views

            t.timestamps null: false
        end
    end
end
