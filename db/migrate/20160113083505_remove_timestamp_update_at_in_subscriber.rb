class RemoveTimestampUpdateAtInSubscriber < ActiveRecord::Migration
    def change
        remove_column :subscribers, :updated_at
    end
end
