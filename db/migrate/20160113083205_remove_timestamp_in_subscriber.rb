class RemoveTimestampInSubscriber < ActiveRecord::Migration
    def change
        remove_column :subscribers, :created_at
        remove_column :subscribers, :updated_at
    end
end
