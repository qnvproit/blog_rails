class CreateAdminTable < ActiveRecord::Migration
    def change
        create_table :admin_tables do |t|
            t.string :username
            t.string :password
            t.string :email
        end
    end
end
