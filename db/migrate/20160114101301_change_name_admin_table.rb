class ChangeNameAdminTable < ActiveRecord::Migration
    def change
        rename_table :admin_tables, :admins
    end
end
