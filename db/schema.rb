# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160116045635) do

  create_table "admins", force: :cascade do |t|
    t.string "username",     limit: 255
    t.string "password",     limit: 255
    t.string "email",        limit: 255
    t.string "access_token", limit: 255
  end

  create_table "categories", force: :cascade do |t|
    t.string "category", limit: 255
  end

  create_table "comments", force: :cascade do |t|
    t.text     "content",           limit: 65535
    t.integer  "post_id",           limit: 4
    t.integer  "parent_comment_id", limit: 4
    t.string   "author",            limit: 255
    t.integer  "status",            limit: 4,     default: 0
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "email",             limit: 255
    t.string   "avatar",            limit: 255
  end

  add_index "comments", ["post_id"], name: "fk_rails_2fd19c0db7", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.text     "content",      limit: 65535
    t.integer  "status",       limit: 4,     default: 0
    t.integer  "category_id",  limit: 4
    t.integer  "number_views", limit: 4,     default: 0
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "posts", ["category_id"], name: "fk_rails_9b1b26f040", using: :btree

  create_table "posts_tags", force: :cascade do |t|
    t.integer "post_id", limit: 4
    t.integer "tag_id",  limit: 4
  end

  add_index "posts_tags", ["post_id"], name: "fk_rails_07fc65c276", using: :btree
  add_index "posts_tags", ["tag_id"], name: "fk_rails_1997ccaabe", using: :btree

  create_table "subscribers", force: :cascade do |t|
    t.string "email",             limit: 255
    t.string "username",          limit: 255
    t.string "followed_category", limit: 255
  end

  create_table "tags", force: :cascade do |t|
    t.string "name_tag", limit: 255
  end

  add_foreign_key "comments", "posts"
  add_foreign_key "posts", "categories"
  add_foreign_key "posts_tags", "posts"
  add_foreign_key "posts_tags", "tags"
end
