function clkLoginBtn() {
    $('.gitkit-sign-in-button button').click();
}

function replyCmt(id, index) {
    $('#parent-cmt').val(id);
    $('#txt-editor-cmt').focus();
    $('html body').animate({
        scrollTop: $('#anchor-editor').offset().top - 80
    });
    $('#content-ctx').html("Replying " + $('#list-cmts #' + id + " .author-cmt").html() + "'s comment (#" + index + ")" );
    $('#context-commenting').show();
}


function getListCmts() {
    $.ajax({
        url: "/comment/list_cmt",
        method: "GET",
        data: {
            post_id: $('#post_id').val()
        },
        success: function(data) {
            console.log(data);
            $('#list-detail-cmts').html('');
            $.each(data, function(index, cmt) {
                addCmtToUI(cmt.id, cmt.author, cmt.content, cmt.avatar, cmt.parent_comment_id, cmt.format_create_at, index);
            })

            $('#txt-editor-cmt').val('');
            $('#loading-cmts').hide();

            $('.tooltipped').tooltip({delay: 50});
        },
        error: function(data) {
            console.log(data);
        }
    })
}


function submitNewCmt() {
    $('#loading-cmts').show();

    if($('#txt-editor-cmt').val().trim().length == 0) {

        return;
    }

    $.ajax({
        url: "/comment/new",
        method: "POST",
        data: {
            parent_comment_id: $('#parent-cmt').val(),
            author: $('.gitkit-account-chip-name').html().trim(),
            post_id: $('#post_id').val(),
            content: $('#txt-editor-cmt').val().trim(),
            avatar: $('.gitkit-account-chip-photo').attr('src'),
            email: $('.gitkit-account-chip-email-label').html().trim(),
        },
        success: function(data) {
            console.log(data);

            showMoreCmt();

            // Get list comments
            getListCmts();
        },
        error: function(data) {
            console.log(data);
            console.log("error");
        }
    })
}




function addCmtToUI(id, author, content, avatar, parent_id, created_at, index) {
    html = "";
    if(parent_id == "" || parent_id == null) {
        html += "<div id='" + id + "' class='comment clearfix'>";    
    } else {
        html += "<div id='" + id + "' class='comment-sub clearfix'>";
    }
    
    html += "<div class=''>";

    avatar_url = (avatar == "" || avatar == null) ? (BASE_URL + "/assets/user.png") : avatar;
    html += "<img src='" + avatar_url + "' class='img-rounder' width='50px' style='float: left' />";

    html += "<div class='cmt-info' style='margin-left: 60px;'>";
    html += "<div class='author-cmt'>" + author + "&nbsp;&nbsp;<small class='pull-right txt-opacity'>" + created_at + "</small>" + "</div>";
    html += "<div class='content-cmt'>" + content;
    html += "<div class='footer-cmt'>";


    if(parent_id == "") {
        html += "<span class='cursor-pointer hover-bright' onclick='replyCmt(" + id + ", " + index + ")'><small>Reply</small></span>";
        html += '<span class="tool-cmt">';
        html += '<i class="fa fa-flag cursor-pointer tooltipped hover-bright" onclick="openModalReport(' + id + ')" data-position="left" data-delay="50" data-tooltip="Report this comment !"></i>&nbsp;&nbsp;|&nbsp;&nbsp;#' + index;    
    } else {
        html += "<span class='cursor-pointer hover-bright' onclick='replyCmt(" + parent_id + ", " + index + ")'><small>Reply</small></span>";
        html += '<span class="tool-cmt">';
        html += '<i class="fa fa-flag cursor-pointer tooltipped hover-bright" data-position="left" data-delay="50" data-tooltip="Report this comment !" onclick="openModalReport(' + id + ')"></i>';
    }

    html += "</span></div></div></div></div>";

    html += "<div class='div-sub-cmt' id='div-sub-" + id + "'>";
    html += "</div>";

    html += "</div>";


    if(parent_id == "" || parent_id == null) {
        $('#list-detail-cmts').append(html);
    } else {
        $('#div-sub-' + parent_id).append(html);
    }

}


function showMoreCmt() {
    $('#list-detail-cmts').css('overflow', 'visible').css('max-height', '100%');
    $('#collapse').hide();
}


function hideContextCommenting() {
    $("#context-commenting").hide();
    $("#parent-cmt").val("");
}

function openModalReport(id) {
    $('#modal-report').openModal();
    $('#report_cmt_id').val(id);
}

function reportCmt() {
    $.ajax({
        url: BASE_URL + "/comment/report",
        method: "POST",
        data: {
            id: $('#report_cmt_id').val()
        },
        success: function(data) {
            if(data.success == true) {
                Materialize.toast('Successfully !', 4000);
                getListCmts();
            } else {
                Materialize.toast('ERROR: ' + data.message, 4000);
            }
        },
        error: function(data) {
            (data.responseText);
        }
    })
}
