function getListPost() {

    $.ajax({
        url: "/post/ajax_list_post",
        method: "GET",
        data: {
            order: 'number_views',
            current_id: $('#list-post .card-panel').length,
            category: $('#category').val(),
            tag: $('#tag').val(),
            search_text: '',
        },
        success: function(data) {
            console.log(data);
            $.each(data, function(index, value) {
                addPostToPage(value);
            })
        },
        error: function(data) {

        }
    })
}

function addPostToPage(post) {

    html = '<div id=' + post.id + ' class="card-panel clearfix">';
    html += '<div class="avatar-post">';
                    
    html += '</div>';
    html += '<div class="post-info">';
    html += '<div class="title-post">';
    html += '<a href="/post/detail/' + post.id + '">'
    html += post.title;
    html += "</a>";
    html += '</div>';
    html += '<div class="created-at">';
    html += '<i class="fa fa-clock-o"></i>&nbsp;&nbsp;' + post.format_create_at;
    html += '</div>';
    html += '<div class="content-post">';
    html += post.short_content;
    html += '</div>';
    html += '<div class="tags-post">';

    $.each(post.tags_array, function(index, value) {
        html += '<div class="sub-tag">';
        html += '<div class="head-tag">';
                                    
        html += '</div>';
        html += '<span class="content-tag">';
        html += value;
        html += '</span>';
        
        html += '</div>&nbsp;&nbsp;';

    })
    
    html += '</div>';                
    html += '</div>';
    html += '</div>';

    $('#list-post').append(html);

}