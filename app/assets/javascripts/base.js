var BASE_URL = "http://localhost:3000";

function scrollToTop() {
    $('html body').animate({
        scrollTop: "0px"
    })
}

function setMessageSubscriber(msg) {
    if(msg !== "") {
        $('#msg-error-subscriber').html(msg);
        $('#subscribe-panel .msg-error').show();
    } else {
        $('#msg-error-subscriber').html("");
        $('#subscribe-panel .msg-error').hide();
    }
}

function subscribeTopic() {
    username = $('#subscriber_name').val().trim();
    if(username.length == 0) {
        setMessageSubscriber("Please fill Your Name !");
        return;
    } else {
        $('#msg-error-subscriber').html("");
    }

    email = $('#subscriber_email').val().trim();
    if(email.length == 0) {
        setMessageSubscriber("Please fill Your Email !");
        return;
    } else {
        pattern = new RegExp(/(\w)+@(\w)+/);
        if(pattern.test(email)) {
            setMessageSubscriber("");
        } else {
            setMessageSubscriber("Please fill correct Email !");
            return;
        }
    }

    arr_selected = $('#select-topic').val();
    category = "";
    $.each(arr_selected, function(index, value) {
        category += "-" + value + "-";
    })
    
    $.ajax({
        url: BASE_URL + "/subscriber/new",
        method: "POST",
        data: {
            username: username,
            email: email, 
            followed_category: category
        },
        success: function(data) {
            console.log(data);
            Materialize.toast(data.message, 4000);
            $('#subscriber_name, #subscriber_email').val('');
        },
        error: function(data) {
            Materialize.toast(data.message, 4000);
        }

    })
}