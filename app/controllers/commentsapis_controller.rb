class CommentsapisController < ApisController


    def get_list_comments
        # begin
        validate_params(params, [:access_token, :page, :limit, :asc, :search])

        query = Comment.unscoped.all

        # search by Post
        query = query.where("post_id = ?", params[:search][:byPostId]) if params[:search][:byPostId] != ""

        # search by Content
        query = query.where("content LIKE ?", "%#{params[:search][:byContent]}%") if params[:search][:byContent] != ""

        # search by Author
        query = query.where("author LIKE ?", "%#{params[:search][:byAuthor]}%") if params[:search][:byAuthor] != ""

        # search by Status
        query = query.where(status: Comment.statuses[params[:search][:byStatus]]) if params[:search][:byStatus] != ""

        # search by Created_at
        query = query.where(:created_at => (params[:search][:byCreated].to_datetime..(params[:search][:byCreated].to_datetime + 1.day))) if params[:search][:byCreated] != ""

        comments = query.order(params[:sortBy] + " " + (params[:asc] ? "ASC" : "DESC")).limit(params[:limit]).offset((params[:page].to_i - 1) * params[:limit].to_i)


        number = query.count

        ajax_ok({:data => comments, :number => number}, "Successfully!")

        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end


    def get_detail_cmt
        begin
            validate_params(params, [:access_token, :id])

            cmt = Comment.find(params[:id])

            ajax_ok(cmt, "Successfully")

        rescue ActiveRecord::RecordNotFound => e
            ajax_error("This comment doesn't exists !")
        # rescue Exception => e
        #     ajax_filter_error(e)
        end
    end


    def set_status_cmt 
        begin
            validate_params(params, [:id, :status]) 

            cmt = Comment.find(params[:id])
            cmt.status = params[:status]
            if cmt.save
                ajax_ok({}, "Successfully")
            else
                ajax_error("Fail to update this comment ! Please try again !")
            end

        rescue ActiveRecord::RecordNotFound => e
            ajax_error("This comment doesn't exists !")

        # rescue Exception => e
        #     ajax_filter_error(e)
        end
    end


    def delete_cmt 
        #begin
        validate_params(params, [:id, :access_token])

        cmt = Comment.destroy(params[:id])
        ajax_ok({}, "Successfully !")
        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end


    def get_list_author
        # begin
        sql = " SELECT email, count(id) as number_cmt, avatar, GROUP_CONCAT(DISTINCT  author SEPARATOR ' / ') as username FROM comments GROUP BY email"

        sql += " ORDER BY " + params[:sortBy] if params[:sortBy] != ""
        sql += (params[:asc]) ? " ASC " : " DESC "
        sql += " LIMIT " + params[:limit].to_s if params[:limit] != ""

        if params[:page] != "" 
            offset = (params[:page].to_i - 1) * params[:limit].to_i
            sql += " OFFSET " + offset.to_s
        end
        
        number = Comment.select('email').distinct.count

        authors = Comment.find_by_sql(sql)

        ajax_ok({ :data => authors, :number => number }, "Successfully")
        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end


end
