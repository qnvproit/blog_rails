class ApisController < ApplicationController 

    before_action :auth_token
    skip_before_action :verify_authenticity_token
    
    around_action :wrap_action


    private 
        def wrap_action
            begin
                yield
            rescue Exception => e
                ajax_filter_error(e)
            end
        end

end