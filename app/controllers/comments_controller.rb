class CommentsController < ApplicationController

    # POST: Add new comment 
    def add_new
        begin 
            cmt = Comment.create(params.permit(:content, :post_id, :author, :email, :avatar, :parent_comment_id))
            render json: {:success => true}
        rescue => e
            render json: {:error => e.message}
        end
    end


    # POST
    def get_list_comments
        begin
            cmts = Comment.where("post_id = ? AND status = ?", params[:post_id], "active")
            render json: cmts.map { |cmt| cmt.to_simple_json}
        rescue => e
            render json: {:error => e.message}
        end
    end


    # POST
    def report_comment
        begin
            cmt = Comment.find(params[:id]).update(status: "reported")
            render json: {:success => true, :message => "Successfully !"}
        rescue => e
            render json: {:success => false, :message => e.message}
        end
    end

end
