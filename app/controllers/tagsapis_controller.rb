class TagsapisController < ApisController

    def get_list_tags
        # begin
        tags = Tag.all.order(:name_tag).pluck(:name_tag)
        ajax_ok(tags, "Successfully")
        # ajax_ok( tags.collect {|tag| tag.name_tag}, "Successfully!")
        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end

    def test
        render plain: request.get?
    end

end
