class PostsController < ApplicationController

    @@number_posts_per_page = 2

    def list_post

        number_posts = Post.count
        @posts = Post.includes(:comment)
                        .limit(@@number_posts_per_page)
                        .offset(0)

        @title = "Have " + number_posts.to_s + " posts"

        # Tags
        @tags = Tag.includes(:posts_tag).all
    end


    def list_post_by_category
        query = Post.includes(:tags).where("category_id = ?", params[:id])
        
        number_posts = query.count
        @posts = query.limit(@@number_posts_per_page).offset(0)
        @category = Category.find(params[:id])
        @tags = Tag.includes(:posts_tag).all

        @title = "Have " + number_posts.to_s + " posts in category '" + @category.category + "'"

        render "posts/list_post"
    end


    def list_post_by_tag

        # query = Tag.includes(:posts, :posts_tag).where("name_tag = ?", "PHP")
        # render json: query[0].posts


        @tag = Tag.includes(:posts).find(params[:id])

    
        number_posts = @tag.posts.count
        @posts = @tag.posts.limit(@@number_posts_per_page).offset(0)

        @tags = Tag.includes(:posts_tag).all

        @title = "Have " + number_posts.to_s + " posts in tag '" + @tag.name_tag + "'"

        render "posts/list_post"
    end


    def ajax_list_post
        order = params[:order]==nil ? :id : params[:order]
        current_id = params[:current_id]==nil ? 0 : params[:current_id]


        if params[:category] != ""
            query = Post.includes(:comment, :tags)
            query = query.where("category_id = ?", params[:category]) 
            @posts = query.limit(@@number_posts_per_page)
                        .offset(current_id)
        elsif params[:tag] != ""
            query = Tag.includes(:posts).find(params[:tag])
            @posts = query.posts.limit(@@number_posts_per_page).offset(current_id)
        else 
            @posts = Post.includes(:comment, :tags).limit(@@number_posts_per_page).offset(current_id)
        end

        render json: @posts.map { |item| item.to_simple_json }
    end


    def get_detail_post
        @post = Post.includes(:comment).find(params[:id])

        @comments = @post.comment.where("status = ?", 'active')

        @posts_tags = PostsTag.includes(:tag).where("post_id = ?", params[:id])

        @tags = Tag.includes(:posts).limit(20)

        # Related post
        @related_post = Post.where("category_id = ? AND id != ?", @post.category_id, @post.id).limit(20)
    end


end
