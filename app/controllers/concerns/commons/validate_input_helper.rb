module ValidateInputHelper


    def validate_params(input, params)
        params.each do |param|
            if input[param].nil?
                # render json: {:success => false, :mesage => "Missing param '#{param}'" }, status: 400 and return
                raise CustomExp::ExpMissingParams.new "Missing '#{param}'"
            end
        end
    end
    
end