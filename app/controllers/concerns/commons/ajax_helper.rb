module AjaxHelper

    def ajax_ok(data, message)
        render json: {
                    :success => true, 
                    :message => message, :data => data
                } and return
    end


    def ajax_error(message)
        render json: {
                    :success => false, 
                    :message => message
                } and return
    end


    def ajax_bad_request(message)
        render json: {
                    :success => false, 
                    :message => message 
                }, status: 400 and return
    end


    def ajax_filter_error(e)
        if e.class.to_s == 'CustomExp::ExpMissingParams'
            render json: {
                        :success => false,
                        :message => e.message
                    }, status: 400 and return
        elsif e.class.to_s == 'CustomExp::ExpUnauthorized'
            render json: {
                        :success => false,
                        :message => e.message
                    }, status: 401 and return

        else
            ajax_server_error(e.message)
        end
            
    end


    def ajax_server_error(message)
        if Rails.env.development?
            render json: {
                        :success => false, 
                        :message => message
                    }, status: 503 and return
        else 
            render json: {
                        :success => false, 
                        :message => "Something is wrong ! Please try again later !"
                    }, status: 503 and return
        end
    end


end