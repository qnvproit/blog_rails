module AdminsCtrlHelper

    def generate_token(admin)
        admin.access_token = Digest::MD5.hexdigest(admin.email + Time.now.to_s)
        if admin.save
            return admin
        else
            return nil
        end
    end

    def auth_token()
        begin
            admin = Admin.find_by_access_token!(params[:access_token])        
        rescue => e
            raise CustomExp::ExpUnauthorized.new "Invalid Access Token !"
        end   
    end
    
end