class PostapisController < ApisController


    def get_list_post
        # begin
        validate_params(params, [:page, :limit, :sortBy, :asc, :access_token])

        query = Post.unscoped.all
        query = query.where("category_id = ?", params[:search][:byCategory]) if  params[:search][:byCategory].present?
        query = query.where(status: Post.statuses[params[:search][:byStatus]]) if params[:search][:byStatus].present?
        query = query.where("title LIKE ?", "%#{params[:search][:byTitle]}%") if params[:search][:byTitle].present?
        query = query.where("content LIKE ?", "%#{params[:search][:byContent]}%") if params[:search][:byContent].present?
        query = query.where(:created_at => params[:search][:byCreated].to_datetime..(params[:search][:byCreated].to_datetime + 1.day)) if params[:search][:byCreated].present?


        posts = query.order(params[:sortBy] + " " + (params[:asc] ? 'asc' : 'desc'))
                    .limit(params[:limit])
                    .offset((params[:page].to_i - 1) * params[:limit].to_i)
        number = query.count

        render json: {:success => true, :data => { :data => posts, :number => number}} and return

        # rescue Exception => e
        #    ajax_filter_error(e)
        # end
    end


    def delete_post
        # begin
        validate_params(params, [:access_token, :id])

        post = Post.destroy(params[:id])

        ajax_ok({}, "Successfully delete this post !")


        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end


    def create_post
        # begin
        validate_params(params, [:title, :content, :status, :category_id, :tags])

        post = Post.create(params.permit(:title, :content, :status, :category_id))

        params[:tags].each do |str|
            tag = Tag.find_or_create_by(:name_tag => str)
            post.tags << tag
        end

        ajax_ok(post, "Successfully create new post !  ")
        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end


    def suggest_post 
        # begin
        validate_params(params, [:text, :access_token])

        posts = Post.where("title LIKE ?", "%#{params[:text]}%").limit(10)

        ajax_ok(posts.collect {|i| {:id => i.id, :title => i.title}}, "Successfully")

        # rescue Exception => e
        #     ajax_filter_error(e)
        # end
    end

end
