class AdminsController < ApplicationController
    # include AjaxHelper
    # skip_before_action :verify_authenticity_token

    def auth
        begin
            admin = Admin.where("email = ? AND password = ?", params[:email], Digest::MD5.hexdigest(params[:password])).first

            if admin.nil?
                ajax_error("Invalid Email or Password !")
            else 
                updated_admin = generate_token(admin)
                if updated_admin.nil?
                    ajax_server_error("Something is wrong !")
                else
                    ajax_ok(updated_admin, "Successfully")
                end
            end
        rescue => e
            ajax_server_error(e.message)
        end
    end

end
