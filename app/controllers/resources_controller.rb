class ResourcesController < ApisController
    require 'socket'
    skip_before_action :auth_token, [:get_list_images]

    def get_list_images
        files = Dir.glob('public/images/post/*')
        response = files.map do |item|
            File.basename(item)
        end
        ajax_ok({
            :hostname => request.protocol + request.host + ":3000" + "/images/post/", 
            :files => response
            }, "Successfully !")
    end


    def get_list_videos
        files = Dir.glob('public/images/post/*')
        ajax_ok(files, "Successfully !")
    end


    def upload_file
        path = File.join("public/images/post/", params[:file].original_filename)
        File.open(path, "wb") { |f| f.write(params[:file].read) }
        ajax_ok({}, "Successfully upload !")
    end

end
