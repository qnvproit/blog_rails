class Comment < ActiveRecord::Base
    belongs_to :post

    enum status: [:active, :pendding, :reported, :deactive]

    default_scope {where status: 'active'}

    def to_simple_json
        self.as_json(
                :except => [:created_at],
                :methods => [:format_create_at]
            )
    end

    def format_create_at
        self.created_at.to_date if not self.created_at.nil?
    end
end
