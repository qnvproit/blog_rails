class Tag < ActiveRecord::Base
    has_many :posts_tag
    has_many :posts, through: :posts_tag
end
