class Post < ActiveRecord::Base
    has_many :comment, dependent: :destroy
    has_many :posts_tags, dependent: :destroy
    has_many :tags, through: :posts_tags
    belongs_to :category

    enum status: {active: 0, pendding: 1, deactive: 2}

    default_scope {where status: 'active'}

    def to_simple_json
        self.as_json(
                :except => [:content, :created_at],
                :methods => [:short_content, :tags_array, :format_create_at]
            )
    end

    
    # Get short content to transfer 
    def short_content
        self.content[0..300]
    end


    def format_create_at
        self.created_at.to_date
    end


    # Get Tags Array of Post
    def tags_array
        self.tags.collect do |item|
            item.name_tag
        end
    end


end
